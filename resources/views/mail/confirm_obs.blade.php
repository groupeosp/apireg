@extends('mail.template')

@section('mail_fields')

    <h2 align="center">Observation validée</h2>
    <p>Bonjour {{ $observation['obs_nom'] }} {{ $observation['obs_prenom'] }}</p>
    <p>Nous vous remercions pour le dépôt de votre observation sur le {{  $observation['termes']['REGISTRE_NOM_L'] }} de {{ $observation['termes']['ENQUETE_PUBLIQUE_L'] }} {{ $observation['reg_titre'] }}</p>
    <p>Celle-ci a été déposée sous le <strong>N°{{ $observation['obs_numero'] }} le {{ $observation['obs_date'] }}</strong></p>
    
    <p><strong>Votre observation : </strong></p>
    <p>{{ $observation['obs_observation'] }}</p>
    @if(array_key_exists('reg_signature_numerique', $observation))
    <p>&nbsp;</p>
    <p><strong>Signature numérique :</strong></p>
    <p>	
        Vous trouverez en pièce-jointe la signature numérique de l'observation déposée que vous êtes le seul à posséder qui garantit l'intégrité de son contenu, 
        cela vous assure que votre observation n'a pas été altérée entre sa signature et sa consultation. Pendant toute la durée de vie du site, vous pouvez le
        vérifier en cliquant sur le lien suivant : 
        <a href="https://www.registredemat.fr/{{ $observation['url_interne'] }}/verification-signature-observation-{{ $observation['obs_numero'] }}" target="_blank">Vérifier l'intégrité de mon observation</a>
    </p>
    @endif
    
@endsection
