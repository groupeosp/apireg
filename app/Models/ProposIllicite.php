<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProposIllicite extends Model
{
    protected $table = 'propos_illicites';
}
