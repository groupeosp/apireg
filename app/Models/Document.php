<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'documents';

    // public function parent()
    // {
    //     return $this->belongsTo(Document::class, 'doc_parent_id')->where('parent_id', 0)->with('parent'); 
        
    // }

    // public function children()
    // {
    //     return $this->hasMany(Document::class, 'doc_parent_id')->with('children'); 
        
    // }

    // static function getChildrens($documents, $idReg)
    //     {
    //         foreach($documents as $document)
    //         {
    //             $childrens = Document::join('registre', 'registre.reg_id', '=', 'documents.reg_id')
    //             ->where('registre.reg_token', '=', $idReg)
    //             ->where('documents.doc_parent_id', '=', $document->doc_id)
    //             ->orderBy('documents.doc_num')
    //             ->orderBy('documents.doc_titre')
    //             ->get();
    //             if(!empty($childrens))
    //             {
    //                 $document->childrens = $childrens; 
    //                 Document::getChildrens($childrens, $idReg); 
    //             }
    //             else
    //             {
    //                 return $documents; 
    //             }
    //         }
    //     }

    // public function documents()
    // {
    //     return $this->hasMany(Document::class, 'doc_parent_id'); 
    // }
    // public function childDocuments()
    // {
    //     return $this->hasMany(Document::class, 'doc_parent_id')->with('documents'); 
    // }
}
