<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    public $timestamps = false;
    protected $table = 'observation';
    protected $fillable = [
        'reg_id',
        'obs_numero', 
        'obs_type', 
        'obs_lieu', 
        'obs_qualite', 
        'obs_nom', 
        'obs_prenom', 
        'obs_email', 
        'obs_diffusion_coord', 
        'obs_adresse', 
        'obs_cp', 
        'obs_ville', 
        'obs_observation',
        'obs_reponse',
        'obs_reponse_informe', 
        'obs_date', 
        'obs_pj_nom', 
        'obs_pj_nom_serveur', 
        'obs_cle_validation',
        'obs_cle_publique',
        'obs_moderation', 
        'obs_ip', 
        'obs_priorite', 
        'obs_avis', 
        'obs_moder_int', 
        'obs_moder_date', 
        'obs_modif_int', 
        'obs_modif_date', 
        'obs_integr_int', 
        'obs_integr_date', 
        'obs_info_pub_rapport', 
        'obs_doublon', 
         
        'obs_mail_id'

    ]; 
}
