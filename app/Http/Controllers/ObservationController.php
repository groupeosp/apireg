<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Registre;
use App\Mail\ConfirmObsMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use App\Models\Observation as Observation;

class ObservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Registre::where("reg_maintenance", 0)->orderBy("reg_id", 'DESC')->skip(0)->take(4)->get();

        return Observation::all();

        // SELECT *
        // FROM `observation`
        // WHERE reg_id = ".securite_bdd($connexion, REGISTRE)."
        // AND obs_numero != 0
        // ".$tri."
        // ORDER BY `obs_numero` DESC ";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //on récupèrele fuseau horaire du registre 
        $fuseau = (Registre::where('reg_id', '=', $request->reg_id)->get('reg_fuseau_horaire')->first())->reg_fuseau_horaire; 
        if($fuseau == '')
        {
            $fuseau = 'Europe/Paris'; 
        }
        //on defini le fuseau horaire 
        date_default_timezone_set($fuseau); 

        //fonction permettant de définir les termes du registre selon le type 
        function getTypeText(int $reg_type)
        {
            switch($reg_type)
            {
                case 0: $enquete_publique = 'enquête publique';
                        $registre_nom = 'registre dématérialisé';
                        $termes = [
                                        'TYPE_REGISTRE' => 0,
                                        'ENQUETE_PUBLIQUE' => $enquete_publique,
                                        'ENQUETE_PUBLIQUE_D' => "d'".$enquete_publique,
                                        'ENQUETE_PUBLIQUE_L' => "l'".$enquete_publique,
                                        'ENQUETE_PUBLIQUE_LINK' => 'enquete-publique',
                                        'REGISTRE_NOM' => $registre_nom,
                                        'REGISTRE_NOM_DL' => 'du '.$registre_nom,
                                        'REGISTRE_NOM_L' => 'le '.$registre_nom,
                                        'COMMISSAIRE_ENQUETEUR' => 'commissaire enquêteur',
                                        'RAPPORT' => 'rapport',
                                        'COMMISSION_ENQUETE' => 'commission d’enquête',
                                        'PERMANENCE' => 'Permanence'
                                        ];break;
                case 1: $enquete_publique = 'concertation';
                        $registre_nom = 'concertation dématérialisée';
                        $termes = [
                                        'TYPE_REGISTRE' => 1,
                                        'ENQUETE_PUBLIQUE' => $enquete_publique,
                                        'ENQUETE_PUBLIQUE_D' => 'de '.$enquete_publique,
                                        'ENQUETE_PUBLIQUE_L' => 'la '.$enquete_publique,
                                        'ENQUETE_PUBLIQUE_LINK' => 'concertation',
                                        'REGISTRE_NOM' => $registre_nom,
                                        'REGISTRE_NOM_DL' => 'de la '.$registre_nom,
                                        'REGISTRE_NOM_L' => 'la '.$registre_nom,
                                        'COMMISSAIRE_ENQUETEUR' => 'garant',
                                        'RAPPORT' => 'bilan',
                                        'COMMISSION_ENQUETE' => 'concertation',
                                        'PERMANENCE' => 'Réunion'
                                        ];break;
                case 2: $enquete_publique = 'PPVE';
                        $registre_nom = 'registre électronique';
                        $termes = [
                                        'TYPE_REGISTRE' => 2,
                                        'ENQUETE_PUBLIQUE' => $enquete_publique,
                                        'ENQUETE_PUBLIQUE_D' => 'de '.$enquete_publique,
                                        'ENQUETE_PUBLIQUE_L' => 'la '.$enquete_publique,
                                        'ENQUETE_PUBLIQUE_LINK' => 'ppve',
                                        'REGISTRE_NOM' => $registre_nom,
                                        'REGISTRE_NOM_DL' => 'du '.$registre_nom,
                                        'REGISTRE_NOM_L' => 'le '.$registre_nom,
                                        'COMMISSAIRE_ENQUETEUR' => 'administrateur',
                                        'RAPPORT' => 'synthese',
                                        'COMMISSION_ENQUETE' => 'PPVE',
                                        'PERMANENCE' => 'Permanence'
                                        ];break;
            }
            return $termes; 
        }

        //fonction permettant d'ajouter une observation 
        function ajouterObs(Request $request)
        {
            $signature =''; 

            //on génère la date d'aujourd'hui dans date puis on la met au format de la bdd dans dateformat
            $date = new DateTime(); 
            $dateformat = $date->format('Y-m-d H:i:s'); 
            
            //on récupère le plus grand obs_numero de la bdd et on ajoute 1
            $numero_suivant = Observation::where('reg_id', $request->reg_id)->max('obs_numero');
            $numero_suivant++ ;

            //on convertit obs_diffusion_coord de str à int 
            if($request->obs_diffusion_coord == "true")
            {
                $anonyme = 1; 
            }
            else{
                $anonyme = 0; 
            }

            //on récupère reg_signature_numerique afin de savoir si le dépot d'observation necessite une signature
            $reg_signature_numerique = Registre::where('reg_id', "=", $request->reg_id)->get('reg_signature_numerique')->first(); 
            // $reg_signature_numerique = 1; 

            //on initialise la cle publique à ''
            $cle_publique = ''; 

            
            
            //on créé une nouvelle observation dans la bdd
            $observation =  Observation::create([
                'reg_id' => $request->reg_id, 
                'obs_numero' => $numero_suivant, 
                // 'obs_numero' => $request->obs_numero, 
                'obs_type' => 0, 
                'obs_lieu' => 0, 
                'obs_qualite' => 0, 
                'obs_nom' => $request->obs_nom, 
                'obs_prenom' => $request->obs_prenom, 
                'obs_email' => $request->obs_email, 
                'obs_diffusion_coord' => $anonyme, 
                'obs_adresse' => $request->obs_adresse, 
                'obs_cp' => "$request->obs_cp", 
                'obs_ville' => $request->obs_ville, 
                'obs_observation' => $request->obs_observation,  
                'obs_reponse' => '', 
                'obs_reponse_informe' => 0, 
                'obs_date' => $dateformat,  
                'obs_pj_nom' => '', 
                'obs_pj_nom_serveur' => '', 
                'obs_cle_validation' => '', 
                'obs_moderation' => 0, 
                'obs_ip' => '', 
                'obs_priorite' => 0, 
                'obs_avis' => 0, 
                'obs_moder_int' => 0, 
                'obs_moder_date' => '0000-00-00 00:00:00',  // voir si le format est bon 
                'obs_modif_int' => 0, 
                'obs_modif_date' => '0000-00-00 00:00:00',  // voir si le format est bon 
                'obs_integr_int' => 0, 
                'obs_integr_date' => '0000-00-00 00:00:00',  // voir si le format est bon 
                'obs_info_pub_rapport' => 1, 
                'obs_doublon' => 1, 
                'obs_mail_id' => '', 
                'obs_cle_publique' =>$cle_publique, 
            ]); 

            

            
            //on vérifie que l'adresse e-mail entrée dans le formulair de l'application est valide
            if(stripos($request->obs_email, '@'))
            {
                //on récupère le type du registre 
                $reg_type = (Registre::where('reg_id', '=', $request->reg_id)->get('reg_type')->first())->reg_type; 

                //on récupère le titre du registre 
                $reg_titre = (Registre::where('reg_id', '=', $request->reg_id)->get('reg_titre')->first())->reg_titre; 

                //si le registre a une signature numerique alors on génère une clé 
                if($reg_signature_numerique)
                {
                    // Algorithme de génération de clé, fournissant une clé privée et publique.
                    $new_key_pair = openssl_pkey_new(array(
                        "private_key_bits" => 2048,
                        "private_key_type" => OPENSSL_KEYTYPE_RSA,
                    ));

                    openssl_pkey_export($new_key_pair, $private_key_pem);
                    $details = openssl_pkey_get_details($new_key_pair);
                    $public_key_pem = $details['key'];

                    // Hacher les données de l'observation
                    $data = hash_hmac('sha256', $request->obs_observation, Config::get('constants.SECRET_KEY'), true);

                    // Algorithme de signature qui combine les données et la clé privée pour créer une signature
                    openssl_sign($data, $signature, $private_key_pem, OPENSSL_ALGO_SHA256);

                    
                    $cle_publique = $public_key_pem; 
                    Observation::where('reg_id', '=', $request->reg_id)->where('obs_numero', '=', $numero_suivant)->update(['obs_cle_publique' => $cle_publique]);

                    //on créé le fichier temporaire contenant la clé publique dans storage
                    Storage::disk('public')->put('observation'.$numero_suivant.'.sign', $signature);

                    //on récupère l'url du registre 
                    $url_interne = Registre::where('reg_id', "=", $request->reg_id)->get('reg_url_interne')->first(); 
                    Mail::to($request->obs_email)->send(new ConfirmObsMail(
                        [
                            'reg_id' => $observation->reg_id, 
                            'reg_signature_numerique' => 1, 
                            'obs_numero' => $observation->obs_numero, 
                            'obs_nom' => $observation->nom, 
                            'obs_prenom' => $observation->prenom, 
                            'obs_date' => date(('d/m/Y H:i:s'), strtotime($observation->obs_date)),  
                            'obs_observation' => $observation->obs_observation, 
                            'obs_cle_publique' => $cle_publique, 
                            // 'signature' => $request->reg_id.'-observation'.$numero_suivant.'.txt', 
                            'signature_file' => 'observation'.$numero_suivant.'.sign', 
                            'url_interne' => $url_interne->reg_url_interne, 
                            'termes' => getTypeText($reg_type), 
                            'reg_titre' => $reg_titre, 
                        ]
                    ));
                    Storage::delete('observation'.$numero_suivant.'.sign');
                } 
                else{
                    Mail::to($request->obs_email)->send(new ConfirmObsMail(
                        [
                            'reg_id' => $observation->reg_id, 
                            'obs_numero' => $observation->obs_numero, 
                            'obs_nom' => $observation->nom, 
                            'obs_prenom' => $observation->prenom, 
                            'obs_date' => $observation->obs_date,  
                            'obs_observation' => $observation->obs_observation, 
                            'termes' => getTypeText($reg_type), 
                            'reg_titre' => $reg_titre, 
                        ]
                    ));
                }
            } 
            return response()->json($observation, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
        }

        return ajouterObs($request); 
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $token)
    {
        $obs_result = Observation::join('registre', 'registre.reg_id', "=", 'observation.reg_id')
        ->leftJoin('lieu_consultation', 'lieu_consultation.lie_id', '=',  'observation.obs_lieu')
            ->where("registre.reg_id", "=", $id)
            ->where("registre.reg_token", "=", $token)
            ->where("observation.obs_numero", "!=", 0)
            ->orderBy("observation.obs_numero", 'DESC')
            ->get();

    //on recupère l'observation pour supprimer les '\_'
    $observation = Observation::where("reg_id", "=", $id)
        ->get('obs_observation')->first(); 
    $observation = stripslashes($observation['obs_observation']); 

    //on recupère le nom pour supprimer les '\_'
    $nom = Observation::where('reg_id', '=', $id)
        ->get('obs_nom')->first(); 
    $nom = stripslashes($nom['obs_nom']); 


    //on recupère le prenom pour supprimer les '\_'
    $prenom = Observation::where('reg_id', '=', $id)
        ->get('obs_prenom')->first(); 
    $prenom = stripslashes($prenom['obs_prenom']); 

    //on recupère la reponse pour supprimer les '\_'
    $reponse = Observation::where('reg_id', '=', $id)
        ->get('obs_reponse')->first(); 
    $reponse = stripslashes($reponse['obs_reponse']); 

        
        if (count($obs_result) == 0) {
            return "Erreur";
        } else {
            return $obs_result->map(function ($element) use ($observation, $nom, $prenom, $reponse) {
                $element->obs_observation = $observation; 
                $element->obs_nom = $nom; 
                $element->obs_prenom = $prenom; 
                $element->obs_reponse = $reponse; 
                return $element;
            });;
        }
    }

    // public function show($id, $token)
    // {
    //     $obs_result = Observation::join('registre', 'registre.reg_id', "=", 'observation.reg_id')
    //     ->where("registre.reg_token", "=", $token)
    //     ->where("observation.reg_id", $id)
    //     ->where("observation.obs_numero", "!=", 0)
    //     ->orderBy("observation.obs_numero", 'DESC')->get();
    //     if (count($obs_result) == 0) {
    //         return "calvitie de Jeremy";
    //     } else {
    //         return $obs_result;
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Observation::findorFail($id); //searching for object in database using ID
        if ($task->delete()) { //deletes the object
            return 'deleted successfully'; //shows a message when the delete operation was successful.
        }
    }
}

