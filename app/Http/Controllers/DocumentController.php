<?php

namespace App\Http\Controllers;

use App\Models\Registre;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Document::all(); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    // //    $documents = Document::with('parent')
    // //     // ->where('doc_parent_id', '=', 0)
    // //     ->where('reg_id', '=', $id)
    // //     ->get()
    // //     ->toArray(); 
    // //     return json_encode($documents); 

    //     $documentsParentsDuReg = Document::join('registre', 'registre.reg_id', '=', 'documents.reg_id')
    //     ->where('registre.reg_token', '=', $id)
    //     ->where('documents.doc_parent_id', '=', '0')
    //     ->orderBy('documents.doc_num')
    //     ->orderBy('documents.doc_titre')
    //     ->get();
    //     $liste = Document::getChildrens($documentsParentsDuReg, $id);
    //     return $liste; 

    //     // $doc = Document::join('registre', 'registre.reg_id', '=', 'documents.reg_id')
    //     // ->where('registre.reg_token', '=', $id)
    //     //     ->where('documents.doc_parent_id', '=', '0')
    //     //     ->orderBy('documents.doc_num')
    //     //     ->orderBy('documents.doc_titre')
    //     //     ->get();
    //     // return  $doc; 

    // }

    public static function parseTree($tree, $root = 0)
    {
        $return = array();
        # Traverse the tree and search for direct children of the root
        foreach ($tree as $child => $parent) { 
            # A direct child is found
            if ($parent == $root) {
                # Remove item from tree (we don't need to traverse this again)
                unset($tree[$child]);
                # Append the child into result array and parse its children
                $return[] = array(
                    'parent' => $child,
                    'children' => self::parseTree($tree, $child),
                    'title' => Document::where('doc_id', $child)->select('doc_titre')->first()->doc_titre, 
                    'id' => Document::where('doc_id', $child)->select('doc_id')->first()->doc_id, 
                    'serveur' => Document::where('doc_id', $child)->select('doc_nom_serveur')->first()->doc_nom_serveur, 
                    'num' => Document::where('doc_id', $child)->select('doc_num')->first()->doc_num, 
                    'date' => Document::where('doc_id', $child)->select('doc_cree_le')->first()->doc_cree_le, 
                );
            }
        }
        return empty($return) ? null : $return;
    }
    function arbre($id, $token)
    {
        $tree = Document::join('registre', 'registre.reg_id', "=", 'documents.reg_id')->where('registre.reg_id', $id)->where('documents.reg_id', $id)->where('registre.reg_token', $token)->get()->pluck('doc_parent_id', 'doc_id');
        $result = self::parseTree($tree);
        // self::printTree($result);
        // var_dump($result); 
       return $result; 
    }
    public static function printTree($tree)
    {
        if (!is_null($tree) && count($tree) > 0) {
            echo '<ul>';
            foreach ($tree as $node) {
                echo '<li>' . $node['parent'] . ' ' . $node['title'];
                self::printTree($node['children']);
                echo '</li>';
            }
            echo '</ul>';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
