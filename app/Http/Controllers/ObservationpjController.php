<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registre;
use App\Models\Observation;
use App\Models\ObservationPj;
use Illuminate\Support\Facades\Storage;

class ObservationpjController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return ObservationPJ::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $token)
    {
        $obs_result = ObservationPJ::join('observation', 'observation.obs_id', "=", 'observation_pj.obs_id')
            ->join('registre', 'registre.reg_id', "=", 'observation.reg_id')
            ->where("registre.reg_token", "=", $token)
            ->where("observation.obs_numero", $id)
            ->orderBy("observation_pj.opj_id", 'ASC')
            ->select('observation_pj.opj_nom', 'observation_pj.opj_id', 'observation_pj.opj_nom_serveur')
            ->get();
        if (count($obs_result) == 0) {
            return "Accès refusé";
        } else {
            return $obs_result;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Observation::findorFail($id); //searching for object in database using ID
        if ($task->delete()) { //deletes the object
            return 'deleted successfully'; //shows a message when the delete operation was successful.
        }
    }
}
