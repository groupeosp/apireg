<?php

namespace App\Http\Controllers;

use App\Models\Registre;
use App\Models\ObservationPersoChamp;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class ChampsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $token)
    {
        return ObservationPersoChamp::join('registre', 'registre.reg_id', "=", 'observation_perso_champs.reg_id')
            ->join('observation_perso_valeurs', 'observation_perso_valeurs.opc_id', "=", 'observation_perso_champs.opc_id')
            ->where("registre.reg_id", "=", $id)
            ->where("registre.reg_token", "=", $token)
            ->select('observation_perso_valeurs.*', 'observation_perso_champs.*')
            ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
