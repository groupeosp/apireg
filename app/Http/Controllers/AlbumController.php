<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Album as Album;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'reg_id' => $request['reg_id'], 
        //     'obs_numero' => '300', 
        //     'obs_nom' => $request['obs_nom'], 
        //     'obs_prenom' => $request['obs_prenom'], 
        //     'obs_email' => $request['obs_email'], 
        //     'obs_adresse' => $request['obs_adresse'], 
        //     'obs_cp' => $request['obs_cp'], 
        //     'obs_ville' => $request['obs_ville'], 
        //     'obs_observation' => $request['obs_observation'],
        // ]); 

        // $observation = new Observation; 
        // $observation->reg_id = (int)$request->reg_id; 
        // $observation->obs_numero = 300; 
        // $observation->obs_nom = $request->obs_nom; 
        // $observation->obs_prenom = $request->obs_prenom; 
        // $observation->obs_email = $request->obs_email; 
        // $observation->obs_adresse = $request->obs_adresse; 
        // $observation->obs_cp = $request->obs_cp; 
        // $observation->obs_ville = $request->obs_ville; 
        // $observation->obs_observation = $request->obs_observation; 


        // $request->json_decode; 
        $observation =  Album::create(
            $request->all()
            //     [
            //     'reg_id' => 6, 
            //     'obs_numero' => 300, 
            //     'obs_nom' => 'bache', 
            //     'obs_prenom' => 'nour', 
            //     'obs_email' => 'email@gmail.com', 
            //     'obs_adresse' => 'adresse', 
            //     'obs_cp' => '95230', 
            //     'obs_ville' => 'soisy', 
            //     'obs_observation' => 'observation éclatééééeee. ', 
            // ]
        );
        return response()->json($observation, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
