<?php

namespace App\Http\Controllers;


use App\Models\Registre;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SousdocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tree = [];
        return Document::getTree('1453', $tree);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listedoc = Document::join('registre', 'registre.reg_id', "=", 'documents.reg_id')
            ->where("registre.reg_token", "=", $id)
            ->orderBy("documents.doc_num", 'DESC')
            ->orderBy("documents.doc_titre", 'DESC')
            ->get();
        // return $listedoc; 
        $listedoc->transform(function ($item) {
            $children = Document::where('doc_parent_id', '=', $item->doc_id)
                ->get();
            if ($children->count() != 0) {
                $item->childrens = $children;
            }
            return $item;
        });
        return $listedoc;
        // return $listedoc; 
        // $listedoc->transform(function ($item) {
        //     $children = Document::where('doc_parent_id', '=', $item->doc_id)
        //         ->get();
        //     if ($children->count() != 0) {
        //         $item->childrens = $children;
        //     }
        //     return $item;
        // });
        // return $listedoc;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public static function parseTree($tree, $root = null)
    {
        $return = array();
        # Traverse the tree and search for direct children of the root
        foreach ($tree as $child => $parent) {
            # A direct child is found
            if ($parent == $root) {
                # Remove item from tree (we don't need to traverse this again)
                unset($tree[$child]);
                # Append the child into result array and parse its children
                $return[] = array(
                    'name' => $child,
                    'children' => self::parseTree($tree, $child),
                    'title' => Document::where('doc_id', $child)->select('doc_titre')->first()->doc_titre
                );
            }
        }
        return empty($return) ? null : $return;
    }
    function arbre($id)
    {
        $tree = Document::where('reg_id', $id)->get()->pluck('doc_parent_id', 'doc_id');
        $result = self::parseTree($tree);
        self::printTree($result);
    }
    public static function printTree($tree)
    {
        if (!is_null($tree) && count($tree) > 0) {
            echo '<ul>';
            foreach ($tree as $node) {
                echo '<li>' . $node['name'] . ' ' . $node['title'];
                self::printTree($node['children']);
                echo '</li>';
            }
            echo '</ul>';
        }
    }
}
