<?php

namespace App\Http\Controllers;

use App\Models\PhotoPresentation;
use Illuminate\Http\Request;
use App\Models\Registre;

class RegistreController extends Controller
{
    public function date_local($id)
    {

        $req = Registre::where("reg_id", "=", $id)->select("reg_fuseau_horaire")->first();
        date_default_timezone_set($req->reg_fuseau_horaire);
        $today = date("Y-m-d H:i:s");
        $reg_result = Registre::where("reg_id", "=", $id)
            ->get();
        foreach ($reg_result as $key => $value) {
            $statut = "";
            if (!$value->reg_fiche)
                $statut = "Attente";
            elseif ($today < $value->reg_ouverture)
                $statut = "Attente";
            elseif ($today >= $value->reg_ouverture && $today <= $value->reg_fermeture)
                $statut = "Ouvert";
            elseif ($today > $value->reg_fermeture) {
                if ($value->reg_date_archivage != '0000-00-00 00:00:00')
                    $date_archivage = $value->reg_date_archivage;
                else {
                    $date_archivage = date_create(substr($value->reg_ouverture, 0, 10));
                    date_modify($date_archivage, '+22 month');
                    $date_archivage = date_format($date_archivage, 'Y-m-d H:i:s');
                }

                if ($today >= $date_archivage)
                    $statut = "Archive";
                else
                    $statut = "Clos";
            } else
                $statut = "Archive";
        }
        return ['statut' => $statut, 'pomme' => 'cactus'];
    }
    //
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $token)
    {
        //on recupère le fuseau horaire du registre 
        $req = Registre::where("reg_id", "=", $id)->where('reg_token', '=', $token)->select("reg_fuseau_horaire")->first();
        if($req->reg_fuseau_horaire != ''){
            date_default_timezone_set($req->reg_fuseau_horaire);
        }else {
            date_default_timezone_set('Europe/Paris');
        }
        $today = date("Y-m-d H:i:s");

        //on récupère le registre 
        $reg_result = Registre::where("registre.reg_id", "=", $id)
        ->where("reg_id", "=", $id)
        ->where("reg_token", "=", $token)
            ->get();
        foreach ($reg_result as $key => $value) {
            $statut = "";
            if (!$value->reg_fiche)
                $statut = "Attente";
            elseif ($today < $value->reg_ouverture)
                $statut = "Attente";
            elseif ($today >= $value->reg_ouverture && $today <= $value->reg_fermeture)
                $statut = "Ouvert";
            elseif ($today > $value->reg_fermeture) {
                if ($value->reg_date_archivage != '0000-00-00 00:00:00')
                    $date_archivage = $value->reg_date_archivage;
                else {
                    $date_archivage = date_create(substr($value->reg_ouverture, 0, 10));
                    date_modify($date_archivage, '+22 month');
                    $date_archivage = date_format($date_archivage, 'Y-m-d H:i:s');
                }

                if ($today >= $date_archivage)
                    $statut = "Archive";
                else
                    $statut = "Clos";
            } else
                $statut = "Archive";
        }

        $photo_presentation = PhotoPresentation::where('reg_id', "=", $id)->get(); //on récupère les photos de présentation
        $title = Registre::where('reg_id', '=', $id)->where('reg_token', '=', $token)->get('reg_titre')->first();  //on récupère le titre du registre
        $title = $title['reg_titre']; 
        $text = Registre::where('reg_id', "=", $id)->where('reg_token', "=", $token)->get('reg_presentation')->first(); //on récupère le texte de présentation
        $regdocpres = Registre::where('reg_id', '=', $id)->where('reg_token', '=', $token)->get('reg_doc_presentation')->first(); 
        
        $text = self::getHtml($text['reg_presentation']);//on retire les balises html du texte de presentation
        $text = stripslashes($text) ; //on retire les '\_' du texte de présentation s'il y en a 
        $title = stripslashes($title);  //on retire les '\_' du titre du registre s'il y en a 
        $regdocpres = stripslashes($regdocpres['reg_doc_presentation']); //on retire les '\_' de reg_doc_pres s'il y en a 


        $req_result = Registre::where("reg_id", "=", $id)
            ->get()->map(function ($element) use ($statut, $photo_presentation, $text, $title, $regdocpres) {
                $element->statut = $statut;
                $element->photo_presentation = $photo_presentation; 
                $element->reg_presentation = $text; 
                $element->reg_titre = $title; 
                $element->reg_doc_presentation = $regdocpres; 
                return $element;
            });
        if (!isset($req_result)) {
            return "pas de registre";
        } else {
            return $req_result;
        }
    }
    
    public function updateCounters(Request $request)
    {
        $registre = Registre::where("reg_id", "=", $request->regid)->first(); 
        $oldCounter = $registre[$request->field]; 
        $newCounter = $oldCounter+1;

        Registre::where("reg_id", "=", $request->regid)->update(array($request->field => $newCounter)); 
        // $registre->update(array('reg_arrete_pdf_count_view' => $newCounter)); 
    }

    // protected function getHtml($text)
    protected function getHtml($text)
    {
        $text = strip_tags($text, null); 
        return $text; 
    }
}
