<?php

namespace App\Mail;

use App\Models\Observation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use PhpParser\Node\Expr\Cast\String_;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\HttpFoundation\Request;

class ConfirmObsMail extends Mailable
{
    use Queueable, SerializesModels;

    //observation attributes 
    public $observation; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->observation = $request; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->observation['signature_file']))
        {
            return $this->subject('Registre Demat : Observation validée')
                    ->from('noreply@legalcom.fr')
                    ->view('mail.confirm_obs')
                    ->attach(storage_path('app/public/'.$this->observation['signature_file']), [
                        // storage_path("app/".$this->observation['signature']), [
                        // 'as' => 'observation'.$this->observation['obs_numero'].'.txt'
                        'as' => $this->observation['signature_file']
                    ]);
                    
        }
        else
        {
            return $this->subject('Observation validée')
                    ->from('noreply@legalcom.fr')
                    ->view('mail.confirm_obs');
        }
        
    }
}
