<?php

use App\Mail\ConfirmObsMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\ChampsController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\RegistreController;
use App\Http\Controllers\ObservationController;
use App\Http\Controllers\ObservationpjController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/obs/{id}/{token}', [ObservationController::class, 'show']);
Route::get('/obspj/{id}/{token}', [ObservationpjController::class, 'show']);

// Route::get('/doc/{id}', [DocumentController::class, 'show']);
Route::get('/arbre/{id}/{token}', [DocumentController::class, 'arbre']);
Route::get('/champs/{id}/{token}', [ChampsController::class, 'show']);
Route::get('/reg/{id}/{token}', [RegistreController::class, 'show']);
Route::get('/maint/{id}', [RegistreController::class, 'date_local']);

Route::get('/index', [DocumentController::class, 'index']);

Route::post('/save/observation', [ObservationController::class, 'store']);

//Route de test 
// Route::post('/album', [AlbumController::class, 'store']);

//changer les compteurs de vues des fichiers pdf des registres 
Route::post('/update/registre', [RegistreController::class, 'updateCounters']);

